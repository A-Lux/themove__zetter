<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'zetter' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.8R:$uq^(-<5t1UHS.S.uxa+#S*}c86dM(yUhGI{9g5NdzGsI]Si:atwt#[wVVDL' );
define( 'SECURE_AUTH_KEY',  'M@CDgmKyz#P=+.B@HvC91T*y($;g2W36J@!sFVUe6OlDU&0wK`k<v>;?3nt`kI>_' );
define( 'LOGGED_IN_KEY',    'c/]A$:`xXXe9umkF.0Y7In!4Es[&jO2_$:-xy2mgMxRI.9M%Oj:YUP54}2`|htgy' );
define( 'NONCE_KEY',        'W9@-<%ogvq#+p%@-g?ncD8e=RQ3s&80vP<!dA$wO/:b#us8zwGW-lX/>O0.pm(&#' );
define( 'AUTH_SALT',        '>0TIq`rqYr$i$>K{NFu;g8eu`;QsQ*3:M)nDi9Phb7v3:OtzZ~#CDvPRY{N|1:`.' );
define( 'SECURE_AUTH_SALT', 'R!s!pyh=?2V@N|[:g u-ykW*{09i<V5X?W=HOr`1l15y%#?npmdCbVr|Zc*A^=mm' );
define( 'LOGGED_IN_SALT',   'uYd~GY%?=YoNiHkUulwaB-M]$*m_jH9n.tF5vj/j)L57|LBw|2}-=o6eX5K[e=H,' );
define( 'NONCE_SALT',       'm4:ws7Ul<1{1DK%{TyBvDq wHz(Np#w4$[dYo(O_,-Xi(IQ1k?Swnb%<mXDq;=t]' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
