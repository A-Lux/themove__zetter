<?php
/**
 * Template part for displaying default posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zetter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('posts-list__item default-item invert-item'); ?>>

	<?php if ( has_post_thumbnail() ) : ?>
		<div class="default-item__thumbnail" <?php zetter_post_overlay_thumbnail( 'zetter-thumb-l' ); ?>></div>
	<?php endif; ?>

	<div class="container">

		<header class="entry-header">
			<div class="entry-meta"><?php
				zetter_posted_by();
				zetter_posted_in( array(
					'prefix' => __( 'In', 'zetter' ),
				) );
				zetter_posted_on( array(
					'prefix' => __( 'Posted', 'zetter' )
				) );
				zetter_post_tags( array(
					'prefix' => __( 'Tags:', 'zetter' )
				) );
				zetter_post_comments( array(
					'postfix' => __( 'Comment(s)', 'zetter' )
				) );
			?></div><!-- .entry-meta -->
			<h3 class="entry-title"><?php 
				zetter_sticky_label();
				the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>' );
			?></h3>
		</header><!-- .entry-header -->

		<?php zetter_post_excerpt(); ?>

		<footer class="entry-footer">
			<div class="entry-meta">
				<?php
					zetter_post_link();
				?>
			</div>
			<?php zetter_edit_link(); ?>
		</footer><!-- .entry-footer -->
	
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
