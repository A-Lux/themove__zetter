<?php
/**
 * Template part for displaying creative posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zetter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'posts-list__item creative-item' ); ?>>

	<?php zetter_post_thumbnail( 'zetter-thumb-m-2' ); ?>

	<div class="creative-item__content">

		<header class="entry-header">
			<h2 class="entry-title"><?php 
				zetter_sticky_label();
				the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>' );
			?></h2>
		</header><!-- .entry-header -->

		<?php zetter_post_excerpt(); ?>

		<footer class="entry-footer">
			<div class="entry-meta"><?php
				zetter_posted_by();
				zetter_posted_in( array(
					'prefix' => __( 'In', 'zetter' ),
				) );
				zetter_posted_on( array(
					'prefix' => __( 'Posted', 'zetter' )
				) );
				zetter_post_tags( array(
					'prefix' => __( 'Tags:', 'zetter' )
				) );
				?><div><?php
					zetter_post_link();
					zetter_post_comments( array(
						'prefix' => '<i class="fa fa-comment" aria-hidden="true"></i>',
						'class'  => 'comments-button'
					) );
				?></div>
			</div>
			<?php zetter_edit_link(); ?>
		</footer><!-- .entry-footer -->
		
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
