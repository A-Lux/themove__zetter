<?php
/**
 * Template part for displaying creative posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zetter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'posts-list__item creative-item invert-hover' ); ?>>

	<?php if ( has_post_thumbnail() ) : ?>
		<div class="creative-item__thumbnail" <?php zetter_post_overlay_thumbnail( 'zetter-thumb-m' ); ?>></div>
	<?php endif; ?>

	<header class="entry-header">
		<?php
			zetter_posted_in();
			zetter_posted_on( array(
				'prefix' => __( 'Posted', 'zetter' )
			) );
		?>
		<h4 class="entry-title"><?php 
			zetter_sticky_label();
			the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>' );
		?></h4>
	</header><!-- .entry-header -->

	<?php zetter_post_excerpt(); ?>

	<footer class="entry-footer">
		<div class="entry-meta">
			<div>
				<?php
					zetter_posted_by();
					zetter_post_comments( array(
						'prefix' => '<i class="fa fa-comment" aria-hidden="true"></i>'
					) );
					zetter_post_tags( array(
						'prefix' => __( 'Tags:', 'zetter' )
					) );
				?>
			</div>
			<?php
				zetter_post_link();
			?>
		</div>
		<?php zetter_edit_link(); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->
