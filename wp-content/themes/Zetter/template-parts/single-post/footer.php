<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zetter
 */

?>

<footer class="entry-footer">
	<div class="entry-meta"><?php
		zetter_post_tags ( array(
			'prefix'    => __( 'Tags:', 'zetter' ),
			'delimiter' => ''
		) );
	?></div>
</footer><!-- .entry-footer -->