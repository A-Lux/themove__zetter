<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zetter
 */

?>

<header class="entry-header">
	<?php the_title( '<h1 class="entry-title h2-style">', '</h1>' ); ?>
	<div class="entry-meta">
		<?php
			zetter_posted_by();
			zetter_posted_in( array(
				'prefix'  => __( 'In', 'zetter' ),
			) );
			zetter_posted_on( array(
				'prefix'  => __( 'Posted', 'zetter' ),
			) );
			zetter_post_comments( array(
				'postfix' => __( 'Comment(s)', 'zetter' ),
			) );
		?>
	</div><!-- .entry-meta -->
</header><!-- .entry-header -->

<?php zetter_post_thumbnail( 'zetter-thumb-l', array( 'link' => false ) ); ?>