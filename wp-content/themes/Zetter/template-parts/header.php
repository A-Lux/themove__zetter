<?php
/**
 * Template part for default Header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Zetter
 */
?>

<?php get_template_part( 'template-parts/top-panel' ); ?>

<div <?php zetter_header_class(); ?>>
	<?php do_action( 'zetter-theme/header/before' ); ?>
	<div class="space-between-content">
		<div <?php echo zetter_site_branding_class(); ?>>
			<?php zetter_header_logo(); ?>
		</div>
		<?php zetter_main_menu(); ?>
	</div>
	<?php do_action( 'zetter-theme/header/after' ); ?>
</div>
