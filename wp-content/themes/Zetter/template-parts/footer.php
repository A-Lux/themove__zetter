<?php
/**
 * The template for displaying the default footer layout.
 *
 * @package Zetter
 */
?>

<?php do_action( 'zetter-theme/widget-area/render', 'footer-area' ); ?>

<div <?php zetter_footer_class(); ?>>
	<div class="space-between-content"><?php
		zetter_footer_copyright();
		zetter_social_list( 'footer' );
	?></div>
</div><!-- .container -->
