<?php
/**
 * Jet Plugins Wizard configuration.
 *
 * @package Zetter
 */
$license = array(
	'enabled' => false,
);

/**
 * Plugins configuration
 *
 * @var array
 */
$plugins = array(
	'jet-data-importer' => array(
		'name'   => esc_html__( 'Jet Data Importer', 'zetter' ),
		'source' => 'remote', // 'local', 'remote', 'wordpress' (default).
		'path'   => 'https://github.com/ZemezLab/jet-data-importer/archive/master.zip',
		'access' => 'base',
	),

	'elementor' => array(
		'name'   => esc_html__( 'Elementor Page Builder', 'zetter' ),
		'access' => 'base',
	),
	'jet-blocks' => array(
		'name'   => esc_html__( 'Jet Blocks For Elementor', 'zetter' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-blocks.zip' ),
		'access' => 'base',
	),
	'jet-blog' => array(
		'name'   => esc_html__( 'Jet Blog For Elementor', 'zetter' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-blog.zip' ),
		'access' => 'base',
	),

	'jet-tabs' => array(
		'name'   => esc_html__( 'Jet Tabs', 'zetter' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-tabs.zip' ),
		'access' => 'base',
	),
	'jet-elements' => array(
		'name'   => esc_html__( 'Jet Elements For Elementor', 'zetter' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-elements.zip' ),
		'access' => 'base',
	),
	
	'jet-theme-core' => array(
		'name'   => esc_html__( 'Jet Theme Core', 'zetter' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-theme-core.zip' ),
		'access' => 'base',
	),
	'contact-form-7' => array(
		'name'   => esc_html__( 'Contact Form 7', 'zetter' ),
		'access' => 'skins',
	),
);

/**
 * Skins configuration
 *
 * @var array
 */
$skins = array(
	'base' => array(
		'jet-data-importer',
		'elementor',
		'jet-blocks',
    	'jet-blog',
		'jet-elements',
		'jet-theme-core',
		'jet-tabs',
	),
	'advanced' => array(
		'default' => array(
			'full'  => array(
				'contact-form-7',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_prod-24034/v1',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/default/default-thumb.jpg',
			'name'  => esc_html__( 'Zetter', 'zetter' ),
		),
	),
);

$texts = array(
	'theme-name' => esc_html__( 'Zetter', 'zetter' ),
);

$config = array(
	'license' => $license,
	'plugins' => $plugins,
	'skins'   => $skins,
	'texts'   => $texts,
);
