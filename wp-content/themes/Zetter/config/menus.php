<?php
/**
 * Menus configuration.
 *
 * @package Zetter
 */

add_action( 'after_setup_theme', 'zetter_register_menus', 5 );
function zetter_register_menus() {

	register_nav_menus( array(
		'main'   => esc_html__( 'Main', 'zetter' ),
		'footer' => esc_html__( 'Footer', 'zetter' ),
		'social' => esc_html__( 'Social', 'zetter' ),
	) );
}
